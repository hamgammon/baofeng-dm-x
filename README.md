# baofeng-dm-x

codeplug and scripts for Baofeng DM-X (1702)

## Buttons

The mode is set to "Channel" which means you see the names.
That's kinda necessary, unless you memorise the order below.
But it also means you can't manual input freq.

The buttons are programmed:

| Button     | Short press | Long press |
|------------|-------------|------------|
| Top        | Battery     | GPS        |
| Upper side | switch A/B  | Zone up    |
| Lower side | switch A/B  | Zone down  |

## General organisation

The DM-X has A and B "VFOs", so I've organised them
as follows:

| Rptrs  | # | A        | B      |
|--------|---|----------|--------|
| 7DE    | 1 | TG9      | TG9    |
| 7EE    | 2 | TG91     | TG2355 |
| 7EA    | 3 | TG92     | TG2350 |
| 7SQ    | 4 | TG93     | R4000  |
|        | 5 | TG9990   | R4400  |
|        | 6 | TG235475 | R4405  |
|        | 7 | TG235444 | R4415  |
|        | 8 | TG23402  | R5000  |
|        | 9 | TG31672  | TG9990 |
|--------|---|----------|--------|
|        | 1 | TG9      | TG9    |
| 7EI    | 2 | TG91     | R4000  |
| 7AB    | 3 | TG92     | R4415  |
| 7II    | 4 | TG93     | R5000  |
|        | 5 | TG9990   | TG9990 |
|--------|---|----------|--------|
| Others | 1 | TG9      | TG9    |
|        | 2 | TG91     | TG9990 |
|        | 3 | TG92     |        |
|        | 4 | TG93     |        |
|        | 5 | TG9990   |        |

"Edinburgh" has interlaced 7DE,7EE
 - though without any TG2*
Everything else is replicated on A and B.            


## TG/reflector key    

|       | TG/reflector key |                    |
|-------|------------------|--------------------|
| both  | TG9              | Local              |
| Slot1 | TG91             | WW                 |
|       | TG92             | EU                 |
|       | TG93             | USA                |
|       | TG9990           | Echo               |
|       | TG235475         | BIChat             |
|       | TG235444         | EdChat             |
|       | TG23402          | D-star link 0058   |
|       | TG31672          | NXDNlink on PiStar |
| Slot2 | TG2355           | Scotland           |
|       | TG2350           | UK                 |
|       | R4000            | Unlink             |
|       | R4400            | UK                 |
|       | R4405            | Scotland           |
|       | R4415            | ScotChat           |
|       | R5000            | Status             |

## My important repeater freqs

| Rptr |  | Rx       | Tx       |
|------|--|----------|----------|
| 7DE  |  | 145.6375 | 145.0375 |
| 7EE  |  | 430.925  | 438.525  |
| 7EA  |  | 439.5375 | 430.5375 |
| 7SQ  |  | 439.675  | 430.675  |
| 7EI  |  | 430.85   | 438.45   |
| 7AB  |  | 439.6625 | 430.6625 |
| 7II  |  | 439.45   | 430.45   |

Note
----

I used https://thisdavej.com/copy-table-in-excel-and-paste-as-a-markdown-table/
to convert the markdown tables from my excel spreadsheet
